import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  transform(araPersonelBilgileri: [], filterText: string) {
    return araPersonelBilgileri ? araPersonelBilgileri.filter(filterText.toString) : [];
  }

}
