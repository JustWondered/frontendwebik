import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonelEklemeComponent } from './personel-ekleme.component';

describe('PersonelEklemeComponent', () => {
  let component: PersonelEklemeComponent;
  let fixture: ComponentFixture<PersonelEklemeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonelEklemeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PersonelEklemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
