import { BoundTarget } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { PersonelBilgileri } from 'src/app/models/personel-bilgileri';
import { PersonelBilgileriService } from 'src/app/services/personel-bilgileri.service';

@Component({
  selector: 'app-personel-ekleme',
  templateUrl: './personel-ekleme.component.html',
  styleUrls: ['./personel-ekleme.component.css']
})
export class PersonelEklemeComponent implements OnInit {
  gorevler:Array<any> = []
  birimler:Array<any> = [
    {birim_ad: "BT", gorev:[
      {pozisyon: "Java", gorev_id:1},
      {pozisyon: ".NET", gorev_id:2},
      {pozisyon: "PHP", gorev_id:3}
      ], birim_id: 1},
    {birim_ad: "Muhasebe",gorev:[{pozisyon:'Muhasebe', gorev_id:4}], birim_id: 2},
    {birim_ad: "Tasarım",gorev:[
      {pozisyon:'Fotoğraf', gorev_id:5},
      {pozisyon: 'Video', gorev_id:6}
    ], birim_id: 3},
  ]
  eklemePersonelBilgileri: PersonelBilgileri
  constructor(private personelBilgileriService:PersonelBilgileriService) { }
  id:number;
  ad:string="";
  soyad:string="";
  cinsiyet:string="";
  tc_kimlik:string="";
  dogum_tarihi:string="";
  dogum_yeri:string="";
  delete:boolean;
  kisisel_bilgiler_id:number=0;
  askerlik:string="";
  bolum:string="";
  cep_telefonu:string="";
  e_posta:string="";
  ikametgah:string="";
  meslek_tanimi:string="";
  ogrenim_durumu:string="";
  tecil_suresi:string="";
  terhis_tarihi:string="";
  yerlesim_adres:string="";
  sgk_id:number
  haftalik_calisma_saati:string="";
  sigorta_bitis_tarihi:string="";
  sozlesme_turu:string="";
  maas:number=0;
  maas_bilgileri_id:number=0;
  is_bilgileri_id:number=0;
  mesai_takibi_id:number=0;
  cikis_tarihi:string="";
  haftalik_izin_gunu:string="";
  ise_giris_tarihi:string="";
  calisma_saati:string="";
  cikis_saati:string=""
  giris_saati:string="";
  gunluk_izin:string="";
  yillik_izin:string="";
  calisma_alani_id:number=0;
  birim_id:number=0;
  gorev_id:number=0;
  pozisyon:string="";
  unvan_adi:string="";
  birim_ad:string="";
  birims:string="";
  gorevs:string="";
  selectedId:number;
  selectedGorevId:number;
  

  ngOnInit(): void {
  }
  birimComboBoxVerileri($event: any, birim_ad: any){
      this.gorevler = this.birimler.find(birim => birim_ad.birim_ad == birim.birim_ad).gorev;

      console.log(this.birimler.find(birim => birim_ad.birim_ad == birim.birim_ad).gorev)
      this.selectedId = birim_ad.birim_id
  
    }
  gorevComboBoxVerileri($event: any,pozisyon: any){
    this.selectedGorevId=pozisyon.gorev_id;
  }

  eklePersonelBilgileri(){
    this.eklemePersonelBilgileri=new PersonelBilgileri();
    this.eklemePersonelBilgileri.ad=this.ad;
    this.eklemePersonelBilgileri.soyad=this.soyad;
    this.eklemePersonelBilgileri.cinsiyet=this.cinsiyet
    this.eklemePersonelBilgileri.tc_kimlik=this.tc_kimlik
    this.eklemePersonelBilgileri.dogum_tarihi=this.dogum_tarihi
    this.eklemePersonelBilgileri.dogum_yeri=this.dogum_yeri
    this.eklemePersonelBilgileri.askerlik=this.askerlik
    this.eklemePersonelBilgileri.bolum=this.bolum
    this.eklemePersonelBilgileri.cep_telefonu=this.cep_telefonu
    this.eklemePersonelBilgileri.e_posta=this.e_posta
    this.eklemePersonelBilgileri.ikametgah=this.ikametgah
    this.eklemePersonelBilgileri.meslek_tanimi=this.meslek_tanimi
    this.eklemePersonelBilgileri.ogrenim_durumu=this.ogrenim_durumu
    this.eklemePersonelBilgileri.tecil_suresi=this.tecil_suresi
    this.eklemePersonelBilgileri.terhis_tarihi=this.terhis_tarihi
    this.eklemePersonelBilgileri.yerlesim_adres=this.yerlesim_adres
    this.eklemePersonelBilgileri.haftalik_calisma_saati=this.haftalik_calisma_saati
    this.eklemePersonelBilgileri.sigorta_bitis_tarihi=this.sigorta_bitis_tarihi
    this.eklemePersonelBilgileri.sozlesme_turu=this.sozlesme_turu
    this.eklemePersonelBilgileri.maas=this.maas
    this.eklemePersonelBilgileri.cikis_tarihi=this.cikis_tarihi
    this.eklemePersonelBilgileri.haftalik_izin_gunu=this.haftalik_izin_gunu
    this.eklemePersonelBilgileri.ise_giris_tarihi=this.ise_giris_tarihi
    this.eklemePersonelBilgileri.calisma_saati=this.calisma_saati
    this.eklemePersonelBilgileri.cikis_saati=this.cikis_saati
    this.eklemePersonelBilgileri.giris_saati=this.giris_saati
    this.eklemePersonelBilgileri.gunluk_izin=this.gunluk_izin
    this.eklemePersonelBilgileri.calisma_alani_id=this.calisma_alani_id
    this.eklemePersonelBilgileri.birim_id=this.selectedId
    this.eklemePersonelBilgileri.gorev_id=this.selectedGorevId
    this.eklemePersonelBilgileri.unvan_adi=this.unvan_adi

    this.personelBilgileriService.eklePersonelBilgileri(this.eklemePersonelBilgileri).subscribe(data => {
      this.eklemePersonelBilgileri = data
    })
  }

}
