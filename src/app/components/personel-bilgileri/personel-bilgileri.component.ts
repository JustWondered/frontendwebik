import { Component, OnInit } from '@angular/core';
import { PersonelBilgileri } from 'src/app/models/personel-bilgileri';
import { PersonelBilgileriService } from 'src/app/services/personel-bilgileri.service';

@Component({
  selector: 'app-personel-bilgileri',
  templateUrl: './personel-bilgileri.component.html',
  styleUrls: ['./personel-bilgileri.component.css'],
})
export class PersonelBilgileriComponent implements OnInit {
  filterText:string="";
  newPersonelBilgileri: PersonelBilgileri[] = [];
  constructor(private personelBilgileriService:PersonelBilgileriService) { }

  ngOnInit(): void {
    this.getPersonelBilgileri();
  }

  getPersonelBilgileri() {
    this.personelBilgileriService.getPersonelBilgileri().subscribe(response => {
      this.newPersonelBilgileri = response
    })
  }
  araPersonelBilgileri(){
    this.personelBilgileriService.araPersonelBilgileri(this.filterText).subscribe(response => {
      this.newPersonelBilgileri=[];
      this.newPersonelBilgileri = response
    })
  }
  silPersonelBilgileri(id:number){
    let silmePersonelBilgileri = this.newPersonelBilgileri.find(item=>{
      return item.id == id
    })
    // = this.newPersonelBilgileri[index]
    silmePersonelBilgileri!.delete=true;
    this.personelBilgileriService.silPersonelBilgileri(silmePersonelBilgileri!).subscribe()
  }
}
