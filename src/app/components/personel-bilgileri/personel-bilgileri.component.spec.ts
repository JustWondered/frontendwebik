import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonelBilgileriComponent } from './personel-bilgileri.component';

describe('PersonelBilgileriComponent', () => {
  let component: PersonelBilgileriComponent;
  let fixture: ComponentFixture<PersonelBilgileriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonelBilgileriComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PersonelBilgileriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
