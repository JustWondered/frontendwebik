import { Component, OnInit } from '@angular/core';
import { PersonelBilgileri } from 'src/app/models/personel-bilgileri';
import { PersonelBilgileriService } from 'src/app/services/personel-bilgileri.service';

@Component({
  selector: 'app-navi',
  templateUrl: './navi.component.html',
  styleUrls: ['./navi.component.css']
})
export class NaviComponent implements OnInit {
  searchText = '';
  aramaPersonelBilgileri: PersonelBilgileri[] = [];
  constructor(private personelBilgileriService:PersonelBilgileriService) { }

  ngOnInit(): void {
  }

}
