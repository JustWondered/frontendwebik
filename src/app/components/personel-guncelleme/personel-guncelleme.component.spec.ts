import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonelGuncellemeComponent } from './personel-guncelleme.component';

describe('PersonelGuncellemeComponent', () => {
  let component: PersonelGuncellemeComponent;
  let fixture: ComponentFixture<PersonelGuncellemeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonelGuncellemeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PersonelGuncellemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
