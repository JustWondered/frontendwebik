import { Component, OnInit } from '@angular/core';
import { PersonelBilgileri } from 'src/app/models/personel-bilgileri';
import { PersonelBilgileriService } from 'src/app/services/personel-bilgileri.service';

@Component({
  selector: 'app-personel-guncelleme',
  templateUrl: './personel-guncelleme.component.html',
  styleUrls: ['./personel-guncelleme.component.css']
})
export class PersonelGuncellemeComponent implements OnInit {
  guncellemePersonelBilgileri: PersonelBilgileri
  constructor(private personelBilgileriService:PersonelBilgileriService) { }
  id:number;
  ad:string="";
  soyad:string="";
  cinsiyet:string="";
  tc_kimlik:string="";
  dogum_tarihi:string="";
  dogum_yeri:string="";
  gorunurluk:boolean = true;
  
  ngOnInit(): void {
  }

  guncellePersonelBilgileri(){
    this.guncellemePersonelBilgileri=new PersonelBilgileri();
    this.guncellemePersonelBilgileri.id=this.id;
    this.guncellemePersonelBilgileri.ad=this.ad;
    this.guncellemePersonelBilgileri.soyad=this.soyad;
    this.guncellemePersonelBilgileri.cinsiyet=this.cinsiyet
    this.guncellemePersonelBilgileri.tc_kimlik=this.tc_kimlik
    this.guncellemePersonelBilgileri.dogum_tarihi=this.dogum_tarihi
    this.guncellemePersonelBilgileri.dogum_yeri=this.dogum_yeri

    this.personelBilgileriService.guncellePersonelBilgileri(this.guncellemePersonelBilgileri).subscribe(data => {
      this.guncellemePersonelBilgileri = data
    })
  }

}
