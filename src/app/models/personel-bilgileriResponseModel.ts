import { PersonelBilgileri } from "./personel-bilgileri";

export interface PersonelBilgileriResponseModel {
    data:PersonelBilgileri[],
}