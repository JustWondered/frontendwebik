import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonelEklemeComponent } from './components/personel-ekleme/personel-ekleme.component';
import { PersonelGuncellemeComponent } from './components/personel-guncelleme/personel-guncelleme.component';

const routes: Routes = [
  { path: 'ekle-personel', component: PersonelEklemeComponent },
  { path: 'guncelle-personel', component: PersonelGuncellemeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
