import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PersonelBilgileri } from '../models/personel-bilgileri';

@Injectable({
  providedIn: 'root'
})
export class PersonelBilgileriService {
  apiUrl = 'http://localhost:8080/api/'
  constructor(private httpClient: HttpClient) { }

  getPersonelBilgileri():Observable<PersonelBilgileri[]>{
    return this.httpClient.get<PersonelBilgileri[]>(this.apiUrl + "kullaniciGetir")
  }
  araPersonelBilgileri(filterText:string):Observable<PersonelBilgileri[]>{
    let options = { headers: new HttpHeaders({ 'ad': filterText.toString() }) };
    return this.httpClient.get<PersonelBilgileri[]>(this.apiUrl + "kullaniciAra",options)
  }
  eklePersonelBilgileri(personelBilgileri:PersonelBilgileri):Observable<any>{
    return this.httpClient.post(this.apiUrl + "kullaniciGirisi",personelBilgileri)
  }
  guncellePersonelBilgileri(personelBilgileri:PersonelBilgileri):Observable<any>{
    return this.httpClient.put(this.apiUrl + "kullaniciGuncelle",personelBilgileri)
  }
  silPersonelBilgileri(personelBilgileri:PersonelBilgileri):Observable<any>{
    return this.httpClient.put(this.apiUrl + "kullaniciGorunurlugu",personelBilgileri.id)
  }
}
