import { TestBed } from '@angular/core/testing';

import { PersonelBilgileriService } from './personel-bilgileri.service';

describe('PersonelBilgileriService', () => {
  let service: PersonelBilgileriService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonelBilgileriService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
