import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoryComponent } from './components/category/category.component';
import { NaviComponent } from './components/navi/navi.component';
import { PersonelBilgileriComponent } from './components/personel-bilgileri/personel-bilgileri.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchFilterPipe } from './components/search-filter.pipe';
import { FormsModule } from '@angular/forms';
import { PersonelEklemeComponent } from './components/personel-ekleme/personel-ekleme.component';
import { PersonelGuncellemeComponent } from './components/personel-guncelleme/personel-guncelleme.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    NaviComponent,
    PersonelBilgileriComponent,
    SearchFilterPipe,
    PersonelEklemeComponent,
    PersonelGuncellemeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
